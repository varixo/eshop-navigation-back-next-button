import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StepService {
  step = 0;
  eventEmitter = new EventEmitter<number>();
}
