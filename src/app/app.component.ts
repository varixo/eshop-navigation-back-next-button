import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { StepService } from './service/step.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'test-eshop-navigation-next-back';

  constructor(private location: Location,
              private stepService: StepService) {
  }


  ngOnInit(): void {
    this.location.subscribe(
      ( (value: PopStateEvent) => {
        let step = value.state.step;
        if (step !== undefined) {
          if (!value.state.isFirstStep) {
            if (!value.state.isBack) {
              step = step + 1;
            } else {
              step = step - 1;
            }
          }
          this.stepService.eventEmitter.emit(step);
        }
      }),
      ( ex => {
        console.log(ex);
      })
    );
  }
}
