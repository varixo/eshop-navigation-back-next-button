import { Component, OnInit } from '@angular/core';
import { StepService } from '../../service/step.service';
import { LocationStrategy } from '@angular/common';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {
  private readonly CURRENT_URL = '/';
  private readonly MAX_STEPS = 10;

  constructor(private location: LocationStrategy,
              private stepService: StepService) { }

  ngOnInit(): void {
    this.location.replaceState({
      step: 0,
      isBack: true,
      isFirstStep: true
    }, 'Step ' + 0, this.CURRENT_URL, '');

    this.stepService.eventEmitter.subscribe((step: number) => {
      this.stepService.step = step;
    });
  }

  get steps(): number[] {
    return Array.from(Array(this.MAX_STEPS).keys());
  }

  get currentStep(): number {
    return this.stepService.step;
  }

  nextStep(): void {
    const currentStep = this.currentStep;
    const state = {
      step: currentStep,
      isBack: false
    };
    this.location.pushState(state, 'Step ' + currentStep, this.CURRENT_URL, '');
    this.stepService.step = (this.currentStep + 1) % this.MAX_STEPS;
  }
}
